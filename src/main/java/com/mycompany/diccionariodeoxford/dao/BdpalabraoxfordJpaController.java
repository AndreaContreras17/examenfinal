/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionariodeoxford.dao;

import com.mycompany.diccionariodeoxford.dao.exceptions.NonexistentEntityException;
import com.mycompany.diccionariodeoxford.dao.exceptions.PreexistingEntityException;
import com.mycompany.diccionariodeoxford.entity.Bdpalabraoxford;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author andre
 */
public class BdpalabraoxfordJpaController implements Serializable {

    public BdpalabraoxfordJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("clientes_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Bdpalabraoxford bdpalabraoxford) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(bdpalabraoxford);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findBdpalabraoxford(bdpalabraoxford.getPalabra()) != null) {
                throw new PreexistingEntityException("Bdpalabraoxford " + bdpalabraoxford + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Bdpalabraoxford bdpalabraoxford) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            bdpalabraoxford = em.merge(bdpalabraoxford);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = bdpalabraoxford.getPalabra();
                if (findBdpalabraoxford(id) == null) {
                    throw new NonexistentEntityException("The bdpalabraoxford with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Bdpalabraoxford bdpalabraoxford;
            try {
                bdpalabraoxford = em.getReference(Bdpalabraoxford.class, id);
                bdpalabraoxford.getPalabra();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bdpalabraoxford with id " + id + " no longer exists.", enfe);
            }
            em.remove(bdpalabraoxford);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Bdpalabraoxford> findBdpalabraoxfordEntities() {
        return findBdpalabraoxfordEntities(true, -1, -1);
    }

    public List<Bdpalabraoxford> findBdpalabraoxfordEntities(int maxResults, int firstResult) {
        return findBdpalabraoxfordEntities(false, maxResults, firstResult);
    }

    private List<Bdpalabraoxford> findBdpalabraoxfordEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Bdpalabraoxford.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Bdpalabraoxford findBdpalabraoxford(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Bdpalabraoxford.class, id);
        } finally {
            em.close();
        }
    }

    public int getBdpalabraoxfordCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Bdpalabraoxford> rt = cq.from(Bdpalabraoxford.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
