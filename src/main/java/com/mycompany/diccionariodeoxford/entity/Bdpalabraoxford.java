/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionariodeoxford.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author andre
 */
@Entity
@Table(name = "bdpalabraoxford")
@NamedQueries({
    @NamedQuery(name = "Bdpalabraoxford.findAll", query = "SELECT b FROM Bdpalabraoxford b"),
    @NamedQuery(name = "Bdpalabraoxford.findByPalabra", query = "SELECT b FROM Bdpalabraoxford b WHERE b.palabra = :palabra"),
    @NamedQuery(name = "Bdpalabraoxford.findBySignifica", query = "SELECT b FROM Bdpalabraoxford b WHERE b.significa = :significa"),
    @NamedQuery(name = "Bdpalabraoxford.findByFecha", query = "SELECT b FROM Bdpalabraoxford b WHERE b.fecha = :fecha")})
public class Bdpalabraoxford implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "palabra")
    private String palabra;
    @Size(max = 2147483647)
    @Column(name = "significa")
    private String significa;
    @Size(max = 2147483647)
    @Column(name = "fecha")
    private String fecha;

    public Bdpalabraoxford() {
    }

    public Bdpalabraoxford(String palabra) {
        this.palabra = palabra;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getSignifica() {
        return significa;
    }

    public void setSignifica(String significa) {
        this.significa = significa;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (palabra != null ? palabra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bdpalabraoxford)) {
            return false;
        }
        Bdpalabraoxford other = (Bdpalabraoxford) object;
        if ((this.palabra == null && other.palabra != null) || (this.palabra != null && !this.palabra.equals(other.palabra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.diccionariodeoxford.entity.Bdpalabraoxford[ palabra=" + palabra + " ]";
    }
    
}
