/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionariodeoxford;

import com.mycompany.diccionariodeoxford.dao.BdpalabraoxfordJpaController;
import com.mycompany.diccionariodeoxford.entity.Bdpalabraoxford;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author andre
 */

@Path ("diccionario")
public class ApiInternaDiccionario {
    
   @GET
   @Produces(MediaType.APPLICATION_JSON)
   public Response significado(){
       
       BdpalabraoxfordJpaController dao= new BdpalabraoxfordJpaController();
       
        List<Bdpalabraoxford>lista= dao.findBdpalabraoxfordEntities();
       
       
       return Response.ok(200).entity(lista).build();

       
   }
        
        
    
    
}
